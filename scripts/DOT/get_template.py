import base64
import os
import requests
import json
from PIL import Image

DOT_URI = os.sys.argv[1]
image_name = os.sys.argv[2]
email = os.sys.argv[3]

def saveFile(path, data):
    f = open(path, "w+")
    f.write(data)
    f.close()    

with open(image_name, "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())
    
    data = requests.post(DOT_URI, json={
        "Username": email,
        "image": {
            "data": encoded_string.decode("utf-8"),
            "faceSizeRatio": {
            "min": 0.05,
            "max": 0.5
            }
            
        },
        "template": True,
        "cropImage": True,
        "facialFeatures": True,
        "icaoAttributes": True,
        "faceAttributes": True
        })
    saveFile("{}.txt".format(image_name), data.content.decode("utf-8"))
    saveFile("{}-template.txt".format(image_name), json.loads(data.content.decode("utf-8"))["faces"][0]["template"])
    saveFile("{}-base64.txt".format(image_name), encoded_string.decode("utf-8"))
