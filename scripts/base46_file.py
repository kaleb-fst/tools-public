import base64
import os
image_name = os.sys.argv[1]
def saveFile(path, data):
    f = open(path, "w+")
    f.write(data)
    f.close()    
with open(image_name, "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())
    saveFile("{}-base64.txt".format(image_name), encoded_string.decode("utf-8"))