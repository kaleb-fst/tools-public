import sys, os
for root, dirs, files in os.walk(sys.argv[1]):
    for file in files:
        print(os.path.join(root, file))
        process = True
        try:
            with open(os.path.join(root, file), "r+") as the_file:
                file_data = the_file.read().expandtabs(4)
        except Exception:
            process = False
        if process:
            os.remove(os.path.join(root, file))
            with open(os.path.join(root, file), "w+") as the_file:
                the_file.write(file_data)
                the_file.close()

