import sys, os
import re
REGEX = r'\/\/ % protected region % .*$'
HEADER = """/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */"""

rgx = re.compile(REGEX)

def clean_text(text: str):
    output = re.sub(REGEX, '', text, flags=re.MULTILINE)
    output = output.replace(HEADER, '')
    return output

for root, dirs, files in os.walk(sys.argv[1]):
    for file in files:
        if ".git" in os.path.join(root, file):
            continue
        print(os.path.join(root, file))
        process = True
        try:
            with open(os.path.join(root, file), "r+") as the_file:
                file_data = clean_text(the_file.read())
        except Exception:
            process = False
        if process:
            os.remove(os.path.join(root, file))
            with open(os.path.join(root, file), "w+") as the_file:
                the_file.write(file_data)
                the_file.close()
